package main

import (
	"github.com/rainycape/memcache"
	"log"
	"time"
)

type MemcachedClient struct {
	mc *memcache.Client
	items chan *Item
	exit chan bool
	ip string
	dry bool
}

func NewMemcachedClient(ip string, timeout time.Duration, workers int, exit chan bool, items chan *Item, dry bool) (client *MemcachedClient, err error) {
	client = &MemcachedClient{}
	client.ip = ip
	client.dry = dry
	client.items = items
	client.mc, err = memcache.New(ip)
	if err != nil {
		return
	}
	client.mc.SetTimeout(timeout)
	client.mc.SetMaxIdleConnsPerAddr(workers)
	return
}

func (client *MemcachedClient) start() {
	log.Printf("Starting memcached client ip: %s", client.ip)
	for {
		select {
		case item := <-client.items:
			if !client.dry {
				if err := client.Set(item); err != nil{
					log.Fatal(err)
				}
			} else {
				log.Printf("Fake set item: %s,  %s", item.Type, item.Id)
			}
		case <-client.exit:
			client.Close()
			return
		}
	}
}

func (client *MemcachedClient) Set(item *Item) (err error) {
	for i := 0; i < 3; i++ {
		err := client.mc.Set(&memcache.Item{Key: item.Id, Value: item.Data})
		if err == nil {
			break
		}
	}
	return
}

func (client *MemcachedClient) Close() {
	client.mc.Close()
}
