package main

import (
	"bufio"
	"compress/gzip"
	"errors"
	"flag"
	"github.com/golang/protobuf/proto"
	"log"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Item struct {
	Type string
	Id   string
	Data []byte
}

var NormalErrRate = 0.01

func processFile(filesQueue chan string, exit chan bool, wg *sync.WaitGroup, memcacheClients map[string]*MemcachedClient){
	for {
		select {
		case file := <-filesQueue:
			log.Printf("Start handeling file: %s", file)
			if errRate, err := handleFile(file, memcacheClients); err != nil {
				log.Printf("Error handeling file: %s", file)
			} else {
				if errRate < NormalErrRate {
					log.Printf("Successfull load file \"%s\" (Error rate: %v)", file, errRate)
				} else {
					log.Printf("Failed load \"%s\".Error rate: %v, more that max limit: %v)", file, errRate, NormalErrRate)
				}

				log.Printf("End handling file %s", file)
			}

			dirPath, fileName := path.Split(file)
			newPath := path.Join(dirPath, "."+fileName)
			err := os.Rename(file, newPath)
			if err != nil {
				log.Printf("Error rename file: %s, %s", file, err)
			} else {
				log.Printf("Renamed file: %s\n", newPath)
			}
			wg.Done()
		case <-exit:
			return
		}
	}
}

func handleFile (filename string, memcacheClients map[string]*MemcachedClient) (errRate float64, err error) {
	file, err := os.Open(filename); if err != nil {return}
	defer file.Close()

	gzReader, err := gzip.NewReader(file); if err != nil {
		return
	}
	defer gzReader.Close()

	var processed, errs uint64 = 0, 0
	scanner := bufio.NewScanner(gzReader)
	for scanner.Scan() {
		line := scanner.Text()
		line = strings.TrimSpace(line)
		item, err := handleOneRecord(line); if err != nil {
			errs++
			log.Println(err)
			continue
		}
		mc := memcacheClients[item.Type]
		if mc == nil {
			errs++
			log.Printf("Unknown device type: %s", item.Type)
			continue
		}
		mc.items <- &item
		processed++
	}

	log.Printf("Proceed %v rows in file %s", processed, filename)
	errRate = float64(errs) / float64(processed)
	err = scanner.Err()
	return
}

func handleOneRecord(s string) (item Item, err error) {
	parts := strings.Split(s, "\t")
	if len(parts) != 5 {
		err = errors.New("Record incorrect format: " + s)
		return
	}
	item.Type = parts[0]
	item.Id = parts[1]

	lat, err := strconv.ParseFloat(parts[2], 64); if err != nil {
		err = errors.New("Incorrect lat in string: " + s)
		return
	}

	lon, err := strconv.ParseFloat(parts[3], 64); if err != nil {
		err = errors.New("Incorrect lat in string: " + s)
		return
	}

	var apps []uint32
	for _, elem := range strings.Split(parts[4], ",") {
		app, err := strconv.ParseUint(elem, 10, 32); if err != nil {
			continue
		}
		apps = append(apps, uint32(app))
	}
	ua := &UserApps {
		Lat: &lat,
		Lon: &lon,
		Apps: apps,
	}
	item.Data, err = proto.Marshal(ua); if err != nil {
		err = errors.New("marshal error")
		return
	}
	return
}

func main() {
	var dry bool
	var logfile, pattern, idfa, gaid, adid, dvid string
	var workers, bufsize int

	flag.StringVar(&pattern, "pattern", "./data/appsinstalled/*.tsv.gz", "")
	flag.StringVar(&logfile, "log", "", "")
	flag.IntVar(&workers, "workers", 5, "")
	flag.IntVar(&bufsize, "bufsize", 10, "")
	flag.StringVar(&idfa, "idfa", "127.0.0.1:33013", "")
	flag.StringVar(&gaid, "gaid", "127.0.0.1:33014", "")
	flag.StringVar(&adid, "adid", "127.0.0.1:33015", "")
	flag.StringVar(&dvid, "dvid", "127.0.0.1:33016", "")
	flag.BoolVar(&dry, "dry", true, "")

	flag.Parse()

	if logfile != "" {
		f, err := os.OpenFile(logfile, os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
		if err != nil {
			log.Fatalf("error opening log file: %v", err)
		}
		defer f.Close()
		log.SetOutput(f)
	}

	clientsExit := make(chan bool)
	defer close(clientsExit)

	memcacheClients := map[string]*MemcachedClient{}
	for devType, ip := range map[string]string{
		"idfa": idfa,
		"gaid": gaid,
		"adid": adid,
		"dvid": dvid,
	} {
		mc, err := NewMemcachedClient(ip, 30 * time.Millisecond, workers, clientsExit, make(chan *Item, 100), dry)
		if err != nil {
			log.Fatal(err)
			return
		}
		memcacheClients[devType] = mc
		go mc.start()
	}

	var files     []string
	var err       error
	var wg sync.WaitGroup

	if files, err = filepath.Glob(pattern); err != nil {
		log.Fatalf("Error glob files with pattern: %s", pattern)
	}

	if files == nil || len(files) == 0 {
		log.Printf("No files for handling with pattern: %s", pattern)
		return
	}

	sort.Strings(files)
	filesQueue := make(chan string, len(files))
	for _, file := range files {
		wg.Add(1)
		filesQueue <- file
	}

	workersExit := make(chan bool)
	defer close(workersExit)

	for i := 0; i < workers; i++ {
		go func() {
			processFile(filesQueue, workersExit, &wg, memcacheClients)
		}()
	}
	wg.Wait()
	log.Printf("Working success")

	// after closing clientsExit and workersExit channels all goroutine finish correctly
}
